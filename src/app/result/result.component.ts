import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.sass']
})
export class ResultComponent implements OnInit {
  @Input() inputedNumber: number;
  @Output() hasError = new EventEmitter<boolean>();
  numberList: number[];
  resultIndex: number;

  constructor() {
    this.hasError.emit(false);
  }

  ngOnInit() {
  }

  execute() {
    if (this.inputedNumber == null || this.inputedNumber < 0 || this.inputedNumber > 100) {
      this.hasError.emit(true);
      return;
    }
    this.hasError.emit(false);

    this.numberList = this.generateNumberList();

    this.numberList.sort((a, b) => {
      return a - b;
    });

    this.resultIndex = this.numberList.indexOf(this.inputedNumber) + 1;
  }

  generateNumberList() {
    const numberList = [this.inputedNumber];
    for (let i = 0, l = 9; i < l; i++) {
      numberList.push(Math.floor(Math.random() * 101));
    }
    return numberList;
  }
}
